import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Traspasobodega } from '../traspasos/traspasobodega';


@Injectable({
  providedIn: 'root'
})
export class TraspasobodegaService {

  private baseEndPoint='http://localhost:8080/api/traspasobodega';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Traspasobodega[]>{
  return this.http.get<Traspasobodega[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Traspasobodega>{
  return this.http.get<Traspasobodega>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(traspasobodega:Traspasobodega):Observable<Traspasobodega>{
  return this.http.post<Traspasobodega>(`${this.baseEndPoint}/ingresar`, traspasobodega);
}

public eliminarTraspasobodega(id: number):Observable<Traspasobodega>{
  return this.http.delete<Traspasobodega>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(traspasobodega:Traspasobodega):Observable<Traspasobodega>{
  return this.http.put<Traspasobodega>(`${this.baseEndPoint}/modificar/${traspasobodega.id}`, traspasobodega);
}

public getDatos(): Observable <Traspasobodega[]>{
  return this.http.get<Traspasobodega[]>(`${this.baseEndPoint}/listar`)
}


}

