export class Traspasobodega {

    id:number;
    cantidadProdTraspaso:number;
    
    producto:{
      
        idProducto:number;
        nombreProducto:string;
      }

      bodegaOrigen:{
      
        id:number;
        nombreBodega:string;
      }

      bodegaDestino:{
      
        id:number;
        nombreBodega:string;
      }

      usuario:{
        idUsuario:number;
        nombreUsuario:string;
      }
}
