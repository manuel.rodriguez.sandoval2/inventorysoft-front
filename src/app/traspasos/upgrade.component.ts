import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import Swal from 'sweetalert2';
import { Bodega } from '../bodega/bodega';
import { BodegaService } from '../bodega/bodega.service';
import { Producto } from '../ingresar/producto';
import { ProductoService } from '../ingresar/producto.service';
import { Traspasobodega } from './traspasobodega';
import { TraspasobodegaService } from './traspasobodega.service';

@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.scss']
})
export class UpgradeComponent implements OnInit {

  disableSelect = new FormControl(false)

  bodegas: Bodega[];
  traspasobodega:Traspasobodega[];
  productos: Producto[];

  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
 totalRegistros=0;
 paginaActual=0;
 totalPorPagina=5;
 pageSizeOptions:number[]=[3,8];



  @ViewChild(MatPaginator, {static: false})paginator: MatPaginator;
  constructor(private fb: FormBuilder, private bodegaService: BodegaService, private traspasobodegaService:TraspasobodegaService,
    private productoService: ProductoService) { }

  ngOnInit():void {
    this.formIngreso.valueChanges.subscribe(console.log);
    this.bodegaService.listar().subscribe((b) => (this.bodegas = b));
    this.productoService.listar().subscribe((p) => (this.productos = p));
    this.calcularRangos();
  }

  formIngreso: FormGroup = this.fb.group({
   
    cantidad: [null, Validators.required],
    idbodegaDestino: [null, Validators.required],
    idbodegaOrigen: [null, Validators.required],
    idProducto: [null, Validators.required],
  });

  paginar(event: PageEvent): void {
    this.paginaActual = event.pageIndex;
    this.totalPorPagina = event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Traspasobodega = new Traspasobodega();


    req.cantidadProdTraspaso = this.formIngreso.value.cantidad;
    req.producto =  {
      idProducto: this.formIngreso.value.idProducto,
      nombreProducto: this.formIngreso.value.nombreProducto
      
    };
    req.bodegaOrigen =  {
      id: this.formIngreso.value.idbodegaOrigen,
      nombreBodega: this.formIngreso.value.nombreBodega
    };
    req.bodegaDestino =  {
      id: this.formIngreso.value.idbodegaDestino,
      nombreBodega: this.formIngreso.value.nombreBodega
    };
  
  

    this.traspasobodegaService.crear(req).subscribe(
      
      (cliente) => {
        console.log(cliente);
        Swal.fire(

          'Documento ingresado con éxito'
        );
        this.ngOnInit();
      },
      
      () => {
        console.log("Completed");
      }
    );
  }

  private calcularRangos() {


    this.traspasobodegaService.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
      .subscribe(p => {
        this.traspasobodega = p.content as Traspasobodega[];
        this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';

      });
      
      
  }
}
