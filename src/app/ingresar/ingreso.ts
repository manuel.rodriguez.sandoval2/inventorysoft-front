
import { ItemIngreso } from './item-ingreso';
import { Producto } from './producto';

export class Ingreso {
  

  id: number;
  numeroDocumento: number;
  fechaIngreso: Date;
  items: Array<ItemIngreso> = [];



  bodega: {
    id: number;
  }
  proveedor: {
    id: number;
  }
  usuario: {
    id: number;
  }

}
