import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../ingresar/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private baseEndPoint='http://localhost:8080/api/producto';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Producto[]>{
  return this.http.get<Producto[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Producto>{
  return this.http.get<Producto>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(producto:Producto):Observable<Producto>{
  return this.http.post<Producto>(`${this.baseEndPoint}/ingresar`, producto);
}

public eliminarBodega(id: number):Observable<Producto>{
  return this.http.delete<Producto>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(producto:Producto):Observable<Producto>{
  return this.http.put<Producto>(`${this.baseEndPoint}/modificar/${producto.idProducto}`, producto);
}

public getDatos(): Observable <Producto[]>{
  return this.http.get<Producto[]>(`${this.baseEndPoint}/listar`)
}


}
