import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingreso } from './ingreso';
import { Producto } from './producto';

@Injectable({
  providedIn: 'root'
})
export class IngresoService {

  private baseEndPoint='http://localhost:8080/api/ingreso';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Ingreso[]>{
  return this.http.get<Ingreso[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Ingreso>{
  return this.http.get<Ingreso>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(ingreso:Ingreso):Observable<Ingreso>{
  return this.http.post<Ingreso>(`${this.baseEndPoint}/ingresar`, ingreso);
}

public eliminarIngreso(id: number):Observable<Ingreso>{
  return this.http.delete<Ingreso>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(ingreso:Ingreso):Observable<Ingreso>{
  return this.http.put<Ingreso>(`${this.baseEndPoint}/modificar/${ingreso.id}`, ingreso);
}

public getDatos(): Observable <Ingreso[]>{
  return this.http.get<Ingreso[]>(`${this.baseEndPoint}/listar`)
}

public buscarProductos(id: number):Observable <any[]>{
  return this.http.get<Producto[]>(`${this.baseEndPoint}/producto/${id}`)

}

obtener(id: number): Observable<any>{
  return this.http.get(`${this.baseEndPoint}/producto/${id}`)
}


}

