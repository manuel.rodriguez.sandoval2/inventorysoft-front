import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Bodega } from '../bodega/bodega';
import { BodegaService } from '../bodega/bodega.service';
import { Proveedor } from '../proveedor/proveedor';
import { ProveedorService } from '../proveedor/proveedor.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Producto } from './producto';
import { ProductoService } from './producto.service';
import { Ingreso } from './ingreso';
import { IngresoService } from './ingreso.service';
import { Observable } from 'rxjs';
import { flatMap, map, startWith } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ItemIngreso } from './item-ingreso';


interface Car {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {


  productos: Producto[];

  ingreso: Ingreso = new Ingreso();
  items: ItemIngreso = new ItemIngreso();
  productos2: Producto = new Producto();
  productosFiltrados: Observable<Producto[]>;
  autocompleteControl = new FormControl();


  bodegas: Bodega[];
  proveedores: Proveedor[];

  constructor(
    private bodegaService: BodegaService,
    private proveedorService: ProveedorService,
    private productoService: ProductoService,
    private ingresoService: IngresoService,
    public http: HttpClient,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private route: Router
  ) { }


  ngOnInit(): void {



    /*  this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('idProducto');
      if (id) {
        this.ingresoService.buscarProductos(id).subscribe((productos) => this.productos = productos);
      }
    });  */
    /*  this.activatedRoute.paramMap.subscribe(params => {
     let id = +params.get('idProducto');
     this.ingresoService.obtener(id).subscribe(
       product => this.productos2 = product[0]);
     
   }); */
    this.autocompleteControl.valueChanges.subscribe(console.log);
    this.bodegaService.listar().subscribe((b) => (this.bodegas = b));
    this.proveedorService.listar().subscribe((p) => (this.proveedores = p));
    // this.productoService.listar().subscribe((pr) => (this.productos = pr));
    this.formIngreso.valueChanges.subscribe(console.log);
    // this.productosFiltrados = this.formIngreso.valueChanges
    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(

        map(value => typeof value === 'number' ? value : value.idProducto),
        flatMap(value => value ? this._filter(value) : [])

      );

  }

  private _filter(id: number): Observable<Producto[]> {
    // const filterValue = value.toLowerCase();

    return this.ingresoService.buscarProductos(id);
  }

  mostrarCodigo(producto?: Producto): number | undefined {

    return producto ? producto.idProducto : undefined;

  }

  mostrarCodigoBodega(bodega?: Bodega): number | undefined {

    return bodega ? bodega.id : undefined;

  }

  mostrarCodigoProveedor(proveedor?: Proveedor): number | undefined {

    return proveedor ? proveedor.id : undefined;

  }





  /* create(ingresoForm): void {
    console.log(this.ingreso);
    if (this.ingreso.items.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid': true });
    }
  
    if (ingresoForm.form.valid && this.ingreso.items.length > 0) {
      this.ingresoService.crear(this.ingreso).subscribe(
        (ingreso) => {
          console.log(ingreso);
          Swal.fire(
  
            'Ingreso de productos realizado con éxito'
        ); 
        
      },
      (error) => {
        console.error(error);
        alert("Error al ingresar");
      },
      () => {
        console.log("Completed");
      }
    );
  }
  } */

  formIngreso: FormGroup = this.fb.group({
    //idProducto: new FormControl(null, Validators.required), 
    idProducto: [null, Validators.required],
    autocompleteControl: [null, Validators.required],
    numeroDocumento: [null, Validators.required],
    bodega: [null, Validators.required],
    proveedor: [null, Validators.required],
    cantidadProducto: [null, Validators.required],
    precio: [null, Validators.required],
    totalPrecio: [null, Validators.required]

  });

  

  crear(): void {
    item: ItemIngreso;
    const req: Ingreso = new Ingreso();
    req.numeroDocumento = this.formIngreso.value.numeroDocumento;
    req.items = [{
      cantidadProducto: this.formIngreso.value.cantidadProducto,
      precio: this.formIngreso.value.precio,
      totalPrecio: this.formIngreso.value.totalPrecio,
      producto: this.formIngreso.value.autocompleteControl,
     // producto: this.formIngreso.value.idProducto,
    }]

   


    req.bodega = {
      id: this.formIngreso.value.bodega
    };
    req.proveedor = {
      id: this.formIngreso.value.proveedor
    };





    this.ingresoService.crear(req).subscribe(
      (ingreso) => {
        console.log(ingreso);
        Swal.fire(

          'Ingreso de productos realizado con éxito'
        );

      },
      (error) => {
        console.error(error);
        alert("Error al ingresar");
      },
      () => {
        console.log("Completed");
      }
    );
    }
  

  cantidadProducto:number=1;
  totalPrecio: number=1;
  precio:number=0;


  onKey(value: number) {    
  
    this.totalPrecio=this.precio*value;

  }



  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    let producto = event.option.value as Producto;
    
    console.log(producto);

    if (this.existeItem(producto.idProducto)) {
      
      this.incrementaCantidad(producto.idProducto);
    } else {
      
      let nuevoItem = new ItemIngreso();
      nuevoItem.producto = producto;
      this.ingreso.items.push(nuevoItem);
    }

    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();
  }

  actualizarCantidad(id: number, event: any): void {
    let cantidad: number = event.target.value as number;
    if (cantidad == 0) {
      return this.eliminarItemIngreso(id);
    }
    this.ingreso.items = this.ingreso.items.map((item: ItemIngreso) => {
      if (id === item.producto.idProducto) {
        item.cantidadProducto = cantidad;
      }
      return item;
    });
  }

  existeItem(id: number): boolean {
    let existe = false;
    this.ingreso.items.forEach((item: ItemIngreso) => {
      if (id === item.producto.idProducto) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.ingreso.items = this.ingreso.items.map((item: ItemIngreso) => {
      if (id === item.producto.idProducto) {
        ++item.cantidadProducto;
      }
      return item;
    });
  }

  eliminarItemIngreso(id: number): void {
    this.ingreso.items = this.ingreso.items.filter((item: ItemIngreso) => id !== item.producto.idProducto);
  }



}
