import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import {MatInputModule} from '@angular/material/input';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { LoginComponent } from './login/login.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { IngresarBodegaComponent } from './bodega/ingresar-bodega.component';
import { MatButtonModule } from '@angular/material/button';
import { EditarBodegaComponent } from './bodega/editar-bodega/editar-bodega.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { IngresarProveedorComponent } from './proveedor/ingresar-proveedor.component';
import { EditarProveedorComponent } from './proveedor/editar-proveedor/editar-proveedor.component';
import { IngresarBajasComponent } from './bajas/ingresar-bajas.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';
import { IngresarClientesComponent } from './clientes/ingresar-clientes.component';
import { FacturaComponent } from './factura/factura.component';
import { MatRippleModule } from '@angular/material/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { TableListComponent } from './movimientos/table-list.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TypographyComponent } from './typography/typography.component';
import { UserProfileComponent } from './usuarios/user-profile.component';
import { UpgradeComponent } from './traspasos/upgrade.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    MatFormFieldModule,
    MatSelectModule ,
    RouterModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatRippleModule,
    MatFormFieldModule,
    CommonModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule, 
    BrowserModule,
    MatRippleModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatFormFieldModule,
    NgbModule,
    
    ToastrModule.forRoot(),
   /* AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRCbn5GIQ5npNwJxaXHZgoOnE_rohJ9nE'    
   })*/
  ],
  
  
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    IngresarComponent,
    LoginComponent,
    IngresarBodegaComponent,
    EditarBodegaComponent,
    EditarUsuarioComponent,
    IngresarProveedorComponent,
    EditarProveedorComponent,
    IngresarBajasComponent,
    IngresarClientesComponent,
    EditarClienteComponent,
    FacturaComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
