import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Usuario } from './usuario';
import { UsuarioService } from './usuario.service';
import { Ciudad } from '../models/ciudad';
import { CiudadService } from '../services/ciudad.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

id:number;
rut:string;
username: string;
nombreUsuario: string;
apellidoPaterno: string;
apellidoMaterno: string;
direccion: string;
email: string;
contrasena: string;
idComuna: number;

permisos = ["Debe seleccionar una opción", "Administrador", "usuario"];


  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
  usuario: Usuario=new Usuario ();
  ciudades:Ciudad[];

  usuarios:Usuario[];
 totalRegistros=0;
 paginaActual=0;
 totalPorPagina=5;
 pageSizeOptions:number[]=[3,8];

 @ViewChild(MatPaginator, {static: false})paginator: MatPaginator;

  constructor(private service:UsuarioService, private router: Router, private serviceCiudad:CiudadService,
    public http: HttpClient,
    private fb: FormBuilder) { }

    formIngreso: FormGroup = this.fb.group({
      rut: [null, Validators.required],
      username: [null, Validators.required],
      nombreUsuario: [null, Validators.required],
      apellidoPaterno: [null, Validators.required],
      apellidoMaterno: [null, Validators.required],
      direccion: [null, Validators.required],
      email: [null, Validators.required],
      contrasena: [null, Validators.required],
      nombreComuna: [null, Validators.required],
      permiso: [null, Validators.required],
      idComuna:[null, Validators.required]
    });

  ngOnInit(): void {
    this.serviceCiudad.listar().subscribe((c) => (this.ciudades = c));
    this.formIngreso.valueChanges.subscribe(console.log);
    this.calcularRangos();
  }

  paginar(event:PageEvent): void{
    this.paginaActual=event.pageIndex;
    this.totalPorPagina=event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Usuario = new Usuario();

    req.rut = this.formIngreso.value.rut;
    req.username = this.formIngreso.value.username;
    req.nombreUsuario = this.formIngreso.value.nombreUsuario;
    req.apellidoPaterno = this.formIngreso.value.apellidoPaterno;
    req.apellidoMaterno = this.formIngreso.value.apellidoMaterno;
    req.direccion = this.formIngreso.value.direccion;
    req.email = this.formIngreso.value.email;
    req.contrasena = this.formIngreso.value.contrasena;
   // req.tipoUsuario = this.formIngreso.value.permiso;
    req.comunas = {
      nombreComuna: this.formIngreso.value.idComuna,
      id: this.formIngreso.value.idComuna
    };
    

    this.service.crear(req).subscribe(
      (usuario) => {
        console.log(usuario);
        Swal.fire(
          
          'Usuario ingresado con éxito'
          );
          this.ngOnInit();
      },
      (error) => {
        console.error(error);
        alert("Rut ingresado ya existe");
      },
      () => {
        console.log("Completed");
      }
    );
  }
  private calcularRangos(){

   
    this.service.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
    .subscribe(p=>{
      this.usuarios=p.content as Usuario[];
      this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';
        
      });
      console.log(Usuario);
  }

  eliminar(usuario:Usuario){
    if(confirm(`¿Seguro que desea eliminar a ${usuario.username}?`))
   console.log(usuario.id,
    usuario.username);
    
    this.service.eliminar(usuario.id).subscribe(()=>{
      this.usuarios=this.usuarios.filter(u=> u!==usuario );
      
      alert(`Usuario ${usuario.username} eliminado con éxito `);
     
    
    });
  }

}
