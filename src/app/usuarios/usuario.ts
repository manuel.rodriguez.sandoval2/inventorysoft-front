export class Usuario {
	
	id:number;
    rut:string;
	nombreUsuario:string;
    apellidoPaterno:string;
	apellidoMaterno:string;
	contrasena:string;
	username:string;
	direccion:string;
	email:string;
	roles:string;
	
	
	//tipoUsuario:string;
	
	comunas:{
      
		id:number;
		nombreComuna:string;
	  }
}
