import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

  usuario: Usuario = new Usuario();
  constructor(private service: UsuarioService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {

      const id: string|number = +params.get('id');
      if (id) {
        this.service.ver(id).subscribe(usuario => {
          this.usuario = usuario;
          console.log(id);
        })
      }
    })
  }

  public editar(): void {
    this.service.editar(this.usuario).subscribe(usuario => {
      console.log(usuario);
      Swal.fire(

        'Datos del usuario se han actualizado con éxito');
      this.router.navigate(['/user-profile']);
    },
      (error) => {
        console.error(error);

      },
      () => {
        console.log("Completed");
      }
    );
  }

}
