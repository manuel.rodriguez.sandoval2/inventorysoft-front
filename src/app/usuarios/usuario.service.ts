import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private baseEndPoint='http://localhost:8080/api/usuario';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Usuario[]>{
  return this.http.get<Usuario[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Usuario>{
  return this.http.get<Usuario>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(usuario:Usuario):Observable<Usuario>{
  return this.http.post<Usuario>(`${this.baseEndPoint}/ingresar`, usuario);
}

public eliminar(id: number):Observable<Usuario>{
  return this.http.delete<Usuario>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(usuario:Usuario):Observable<Usuario>{
  return this.http.put<Usuario>(`${this.baseEndPoint}/modificar/${usuario.id}`, usuario);
}

public getDatos(): Observable <Usuario[]>{
  return this.http.get<Usuario[]>(`${this.baseEndPoint}/listar`)
}


}
