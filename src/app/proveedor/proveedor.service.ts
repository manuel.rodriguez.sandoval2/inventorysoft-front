import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Proveedor } from './proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  private baseEndPoint='http://localhost:8080/api/proveedor';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Proveedor[]>{
  return this.http.get<Proveedor[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Proveedor>{
  return this.http.get<Proveedor>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(proveedor:Proveedor):Observable<Proveedor>{
  return this.http.post<Proveedor>(`${this.baseEndPoint}/ingresar`, proveedor);
}

public eliminar(id: number):Observable<Proveedor>{
  return this.http.delete<Proveedor>(`${this.baseEndPoint}/eliminar/`+id);
}

public editar(proveedor:Proveedor):Observable<Proveedor>{
  return this.http.put<Proveedor>(`${this.baseEndPoint}/modificar/${proveedor.id}`, proveedor);
}

public getDatos(): Observable <Proveedor[]>{
  return this.http.get<Proveedor[]>(`${this.baseEndPoint}/listar`)
}


}
