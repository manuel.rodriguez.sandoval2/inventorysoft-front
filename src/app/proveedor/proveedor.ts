export class Proveedor {

    id:number;
    rut:string;
	nombreProveedor:string;
    direccion:string;
	email:string;
    giro:string;
    
    comunas:{
      
		id:number;
		nombreComuna:string;
	  }
}
