import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Ciudad } from '../models/ciudad';
import { CiudadService } from '../services/ciudad.service';
import { Proveedor } from './proveedor';
import { ProveedorService } from './proveedor.service';

@Component({
  selector: 'app-ingresar-proveedor',
  templateUrl: './ingresar-proveedor.component.html',
  styleUrls: ['./ingresar-proveedor.component.css']
})
export class IngresarProveedorComponent implements OnInit {

id:number;
nombreProveedor:string;
direccion: string;
email: string;
giro: string;
descProveedor:string;


  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
  proveedor: Proveedor=new Proveedor ();

  ciudades:Ciudad[];

  proveedors:Proveedor[];
 totalRegistros=0;
 paginaActual=0;
 totalPorPagina=5;
 pageSizeOptions:number[]=[3,8];

 @ViewChild(MatPaginator, {static: false})paginator: MatPaginator;

  constructor(private service:ProveedorService, private router: Router, private serviceCiudad:CiudadService,
    public http: HttpClient,
    private fb: FormBuilder) { }

    formIngreso: FormGroup = this.fb.group({
      rut: [null, Validators.required],
      nombreProveedor: [null, Validators.required],
      direccion: [null, Validators.required],
      email: [null, Validators.required],
      giro: [null, Validators.required],
      idComuna: [null, Validators.required],
    });

  ngOnInit(): void {
    this.serviceCiudad.listar().subscribe((c) => (this.ciudades = c));
    this.formIngreso.valueChanges.subscribe(console.log);
    this.calcularRangos();
  }

  paginar(event:PageEvent): void{
    this.paginaActual=event.pageIndex;
    this.totalPorPagina=event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Proveedor = new Proveedor();

    req.rut = this.formIngreso.value.rut;
    req.nombreProveedor = this.formIngreso.value.nombreProveedor;
    req.direccion = this.formIngreso.value.direccion;
    req.email = this.formIngreso.value.email;
    req.giro = this.formIngreso.value.giro;
    req.comunas = {
      nombreComuna: this.formIngreso.value.nombreComuna,
      id: this.formIngreso.value.idComuna
    };
    

    this.service.crear(req).subscribe(
      (proveedor) => {
        console.log(proveedor);
        Swal.fire(
          
          'Proveedor ingresado con éxito'
          );
          this.ngOnInit();
      },
      (error) => {
        console.error(error);
        alert("Rut ingresado ya existe");
      },
      () => {
        console.log("Completed");
      }
    );
  }
  private calcularRangos(){

   
    this.service.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
    .subscribe(p=>{
      this.proveedors=p.content as Proveedor[];
      this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';
        
      });
      console.log(Proveedor);
  }

  eliminar(proveedor:Proveedor){
    if(confirm(`¿Seguro que desea eliminar a ${proveedor.nombreProveedor}?`))
   console.log(proveedor.id,
    proveedor.nombreProveedor);
    
    this.service.eliminar(proveedor.id).subscribe(()=>{
      this.proveedors=this.proveedors.filter(u=> u!==proveedor );
      
      alert(`Proveedor ${proveedor.nombreProveedor} eliminado con éxito `);
     
    
    });
  }

}