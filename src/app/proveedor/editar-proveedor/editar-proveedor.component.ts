import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Proveedor } from '../proveedor';
import { ProveedorService } from '../proveedor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-proveedor',
  templateUrl: './editar-proveedor.component.html',
  styleUrls: ['./editar-proveedor.component.css']
})
export class EditarProveedorComponent implements OnInit {

  proveedor: Proveedor = new Proveedor();
  constructor(private service: ProveedorService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {

      const id: string|number = +params.get('id');
      if (id) {
        this.service.ver(id).subscribe(proveedor => {
          this.proveedor = proveedor;
          console.log(id);
        })
      }
    })
  }

  public editar(): void {
    this.service.editar(this.proveedor).subscribe(proveedor => {
      console.log(proveedor);
      Swal.fire(

        'Datos del proveedor se han actualizado con éxito');
      this.router.navigate(['/ingresar-proveedor']);
    },
      (error) => {
        console.error(error);

      },
      () => {
        console.log("Completed");
      }
    );
  }

}
