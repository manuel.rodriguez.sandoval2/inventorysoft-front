import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  cliente: Cliente = new Cliente();
  constructor(private service: ClienteService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {

      const id: string|number = +params.get('id');
      if (id) {
        this.service.ver(id).subscribe(cliente => {
          this.cliente = cliente;
          console.log(id);
        })
      }
    })
  }

  public editar(): void {
    this.service.editar(this.cliente).subscribe(cliente => {
      console.log(cliente);
      Swal.fire(

        'Datos del cliente se han actualizado con éxito');
      this.router.navigate(['/ingresar-cliente']);
    },
      (error) => {
        console.error(error);

      },
      () => {
        console.log("Completed");
      }
    );
  }

}

