export class Cliente {


    id:number;
    rutCliente:string;
    descCliente:string;
    direccion:string;
    nombreCliente:string;
    apellidoPaterno:string;
    apellidoMaterno:string;
    email:string;
    
    comunas:{
      
        id:number;
        nombreComuna:string;
      }
    }