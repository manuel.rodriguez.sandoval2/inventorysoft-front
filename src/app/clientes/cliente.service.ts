import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../clientes/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private baseEndPoint='http://localhost:8080/api/cliente';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Cliente[]>{
  return this.http.get<Cliente[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Cliente>{
  return this.http.get<Cliente>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(cliente:Cliente):Observable<Cliente>{
  return this.http.post<Cliente>(`${this.baseEndPoint}/ingresar`, cliente);
}

public eliminar(id: number):Observable<Cliente>{
  return this.http.delete<Cliente>(`${this.baseEndPoint}/eliminar/`+id);
}

public editar(cliente:Cliente):Observable<Cliente>{
  return this.http.put<Cliente>(`${this.baseEndPoint}/modificar/${cliente.id}`, cliente);
}

public getDatos(): Observable <Cliente[]>{
  return this.http.get<Cliente[]>(`${this.baseEndPoint}/listar`)
}


}

