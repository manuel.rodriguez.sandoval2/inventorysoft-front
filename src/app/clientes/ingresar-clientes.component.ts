import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Ciudad } from '../models/ciudad';
import { ClienteService } from './cliente.service';
import { Cliente } from './cliente';
import { CiudadService } from '../services/ciudad.service';

@Component({
  selector: 'app-ingresar-clientes',
  templateUrl: './ingresar-clientes.component.html',
  styleUrls: ['./ingresar-clientes.component.css']
})
export class IngresarClientesComponent implements OnInit {


  rutCliente: string;
  nombreCliente: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  direccion: string;
  email: string;
  descCliente: string;
  nombreComuna: string;
  idComuna: number;





  cliente: Cliente = new Cliente();
  ciudades: Ciudad[];

  clientes: Cliente[];

  opcionSeleccionado: string = '0';
  verSeleccion: string = '';
  totalRegistros = 0;
  paginaActual = 0;
  totalPorPagina = 5;
  pageSizeOptions: number[] = [3, 8];

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private service: ClienteService, private router: Router, private serviceCiudad: CiudadService,
    public http: HttpClient,
    private fb: FormBuilder) { }

  formIngreso: FormGroup = this.fb.group({

    rutCliente: [null, Validators.required],
    nombreCliente: [null, Validators.required],
    apellidoPaterno: [null, Validators.required],
    apellidoMaterno: [null, Validators.required],
    direccion: [null, Validators.required],
    email: [null, Validators.required],
    descCliente: [null, Validators.required],
    nombreComuna: [null, Validators.required],
    idComuna: [null, Validators.required]
  });

  ngOnInit(): void {
    this.serviceCiudad.listar().subscribe((c) => (this.ciudades = c));
    this.formIngreso.valueChanges.subscribe(console.log);
    this.calcularRangos();
  }

  paginar(event: PageEvent): void {
    this.paginaActual = event.pageIndex;
    this.totalPorPagina = event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Cliente = new Cliente();


    req.rutCliente = this.formIngreso.value.rutCliente;
    req.nombreCliente = this.formIngreso.value.nombreCliente;
    req.apellidoPaterno = this.formIngreso.value.apellidoPaterno;
    req.apellidoMaterno = this.formIngreso.value.apellidoMaterno;
    req.direccion = this.formIngreso.value.direccion;
    req.email = this.formIngreso.value.email;
    req.descCliente = this.formIngreso.value.descCliente;
    req.comunas = {
      nombreComuna: this.formIngreso.value.idComuna,
      id: this.formIngreso.value.idComuna
    };


    this.service.crear(req).subscribe(
      (cliente) => {
        console.log(cliente);
        Swal.fire(

          'Cliente ingresado con éxito'
        );
        this.ngOnInit();
      },
      (error) => {
        console.error(error);
        alert("Rut ingresado ya existe");
      },
      () => {
        console.log("Completed");
      }
    );
  }
  private calcularRangos() {


    this.service.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
      .subscribe(p => {
        this.clientes = p.content as Cliente[];
        this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';

      });
    console.log(Cliente);
  }

  eliminar(cliente: Cliente) {
    if (confirm(`¿Seguro que desea eliminar a ${cliente.nombreCliente}?`))
      console.log(cliente.id,
        cliente.nombreCliente);

    this.service.eliminar(cliente.id).subscribe(() => {
      this.clientes = this.clientes.filter(u => u !== cliente);

      alert(`Cliente ${cliente.nombreCliente} eliminado con éxito `);


    });
  }

}
