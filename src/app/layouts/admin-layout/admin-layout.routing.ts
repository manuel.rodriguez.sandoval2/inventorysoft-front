import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../usuarios/user-profile.component';
import { TableListComponent } from '../../movimientos/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../traspasos/upgrade.component';
import { IngresarComponent } from '../../ingresar/ingresar.component';
import { IngresarBodegaComponent } from '../../bodega/ingresar-bodega.component';
import { EditarBodegaComponent } from '../../bodega/editar-bodega/editar-bodega.component';
import { EditarUsuarioComponent } from '../../usuarios/editar-usuario/editar-usuario.component';
import { IngresarProveedorComponent } from '../../proveedor/ingresar-proveedor.component';
import { EditarProveedorComponent } from '../../proveedor/editar-proveedor/editar-proveedor.component';
import { IngresarBajasComponent } from '../../bajas/ingresar-bajas.component';
import { IngresarClientesComponent } from '../../clientes/ingresar-clientes.component';
import { EditarClienteComponent } from '../../clientes/editar-cliente/editar-cliente.component';
import { FacturaComponent } from '../../factura/factura.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'ingresar',        component: IngresarComponent },
    { path: 'ingresar/:idProducto',        component: IngresarComponent },
    { path: 'ingresar-bodega', component: IngresarBodegaComponent },
    { path: 'ingresar-cliente', component: IngresarClientesComponent },
    { path: 'ingresar-cliente/editar-cliente/:id', component: EditarClienteComponent },
    { path: 'ingresar-bodega/editar-bodega/:id', component: EditarBodegaComponent },
    { path: 'user-profile/editar-usuario/:id', component: EditarUsuarioComponent },
    { path: 'ingresar-proveedor', component: IngresarProveedorComponent },
    { path: 'ingresar-proveedor/editar-proveedor/:id', component: EditarProveedorComponent },
    { path: 'ingresar-bajas', component: IngresarBajasComponent },
    { path: 'factura', component: FacturaComponent }
];
