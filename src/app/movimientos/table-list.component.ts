import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
 totalRegistros=0;
 paginaActual=0;
 totalPorPagina=5;
 pageSizeOptions:number[]=[3,8];

  constructor() { }

  ngOnInit() {
  }

}
