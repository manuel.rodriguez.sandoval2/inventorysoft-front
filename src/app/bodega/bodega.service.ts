import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bodega } from '../bodega/bodega';

@Injectable({
  providedIn: 'root'
})
export class BodegaService {

  private baseEndPoint='http://localhost:8080/api/bodega';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Bodega[]>{
  return this.http.get<Bodega[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Bodega>{
  return this.http.get<Bodega>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(bodega:Bodega):Observable<Bodega>{
  return this.http.post<Bodega>(`${this.baseEndPoint}/ingresar`, bodega);
}

public eliminarBodega(id: number):Observable<Bodega>{
  return this.http.delete<Bodega>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(bodega:Bodega):Observable<Bodega>{
  return this.http.put<Bodega>(`${this.baseEndPoint}/modificar/${bodega.id}`, bodega);
}

public getDatos(): Observable <Bodega[]>{
  return this.http.get<Bodega[]>(`${this.baseEndPoint}/listar`)
}


}
