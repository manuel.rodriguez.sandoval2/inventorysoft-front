import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bodega } from '../bodega';
import { BodegaService } from '../bodega.service';
import Swal from 'sweetalert2';
import { Ciudad } from '../../models/ciudad';
import { CiudadService } from '../../services/ciudad.service';

@Component({
  selector: 'app-editar-bodega',
  templateUrl: './editar-bodega.component.html',
  styleUrls: ['./editar-bodega.component.css']
})
export class EditarBodegaComponent implements OnInit {

  ciudades:Ciudad[];
  bodega: Bodega = new Bodega();
  
  
  constructor(private service: BodegaService, private serviceCiudad:CiudadService, private route: ActivatedRoute, 
    private router: Router ) { }

    
  ngOnInit(){
    this.serviceCiudad.listar().subscribe((c) => (this.ciudades = c));
    this.route.paramMap.subscribe(params => {

      const id: number = +params.get('id');
      if (id) {
        
        this.service.ver(id).subscribe(bodega => {
          this.bodega = bodega;
          console.log(id);
        })
      }
    })
    
  }

  public editar(): void {
    this.service.editar(this.bodega).subscribe(bodega => {
      console.log(bodega);
      Swal.fire(

        'Datos de bodega se han actualizado con éxito');
      this.router.navigate(['/ingresar-bodega']);
    },
      (error) => {
        console.error(error);

      },
      () => {
        console.log("Completed");
      }
    );
  }

}
