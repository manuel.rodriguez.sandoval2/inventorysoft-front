import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Bodega } from './bodega';
import { BodegaService } from './bodega.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { Ciudad } from '../models/ciudad';
import { CiudadService } from '../services/ciudad.service';


@Component({
  selector: 'app-ingresar-bodega',
  templateUrl: './ingresar-bodega.component.html',
  styleUrls: ['./ingresar-bodega.component.css']
})
export class IngresarBodegaComponent implements OnInit {

codigo: number;
nombreBodega: string;
direccion: string;
idCiudad:number;

  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
  bodega: Bodega=new Bodega ();


  bodegas:Bodega[];
  ciudades:Ciudad[];

 totalRegistros=0;
 paginaActual=0;
 totalPorPagina=5;
 pageSizeOptions:number[]=[3,8];

 @ViewChild(MatPaginator, {static: false})paginator: MatPaginator;

  constructor(private service:BodegaService, private router: Router, private serviceCiudad:CiudadService,
    public http: HttpClient,
    private fb: FormBuilder) { }

    formIngreso: FormGroup = this.fb.group({
      id: [null, Validators.required],
      nombreBodega: [null, Validators.required],
      direccion: [null, Validators.required],
      idComuna: [null, Validators.required],
      
    });

  ngOnInit(): void {
    this.serviceCiudad.listar().subscribe((c) => (this.ciudades = c));
    this.formIngreso.valueChanges.subscribe(console.log);
    this.calcularRangos();
  }

  paginar(event:PageEvent): void{
    this.paginaActual=event.pageIndex;
    this.totalPorPagina=event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Bodega = new Bodega();

    req.id = this.formIngreso.value.id;
    req.nombreBodega = this.formIngreso.value.nombreBodega;
    req.direccion = this.formIngreso.value.direccion;
    req.comunas = {
      nombreComuna: this.formIngreso.value.nombreComuna,
      id: this.formIngreso.value.idComuna
    };
    

    this.service.crear(req).subscribe(
      (bodega) => {
        console.log(bodega);
        Swal.fire(
          
          'Bodega ingresada con éxito'
          );
          this.ngOnInit();
      },
      (error) => {
        console.error(error);
        alert("Código ya existe");
      },
      () => {
        console.log("Completed");
      }
    );
  }
  private calcularRangos(){

   
    this.service.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
    .subscribe(p=>{
      this.bodegas=p.content as Bodega[];
      this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';
        
      });
      console.log(Bodega);
  }

  eliminarBodega(bodega:Bodega){
    if(confirm(`¿Seguro que desea eliminar a ${bodega.nombreBodega}?`))
   console.log(bodega.id,
    bodega.nombreBodega);
    
    this.service.eliminarBodega(bodega.id).subscribe(()=>{
      this.bodegas=this.bodegas.filter(u=> u!==bodega );
      
      alert(`Bodega ${bodega.nombreBodega} eliminada con éxito `);
     
    
    });
  }

}
