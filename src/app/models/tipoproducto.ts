export class Tipoproducto {

    idTipoProducto:number;
    nombreTipoProducto:string;
    unidadMedidad:string;
    
    productos:{
      
        idProducto:number;
        nombreProducto:string;
      }
}
