import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [    
    { path: '/notifications', title: 'Notificaciones',  icon:'ui-1_bell-53', class: '' },
    { path: '/ingresar', title: 'Ingresar',  icon:'ui-1_simple-add', class: '' },
   // { path: '/maps', title: 'Mapas',  icon:'location_map-big', class: '' },    
    { path: '/user-profile', title: 'Administración Usuarios',  icon:'business_badge', class: '' },
    { path: '/ingresar-proveedor', title: 'Administración Proveedores',  icon:'shopping_delivery-fast', class: '' },
    { path: '/ingresar-bodega', title: 'Administración Bodegas',  icon:'business_bank', class: '' },
    { path: '/ingresar-cliente', title: 'Administración Clientes',  icon:'users_single-02', class: '' },
    { path: '/ingresar-bajas', title: 'Administración Bajas',  icon:'ui-1_simple-remove', class: '' },    
    { path: '/table-list', title: 'Movimientos',  icon:'arrows-1_refresh-69', class: '' },
    { path: '/upgrade', title: 'Traspasos',  icon:'sport_user-run', class: '' },
    { path: '/typography', title: 'Consultar Stock',  icon:'ui-1_zoom-bold', class: '' },
    { path: '/dashboard', title: 'Dashboard',  icon:'business_chart-bar-32', class: '' },
    { path: '/factura', title: 'Salidas',  icon:'files_paper', class: '' },
    { path: '/icons', title: 'Iconos',  icon:'ui-1_simple-add', class: '' }
    

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };
}
