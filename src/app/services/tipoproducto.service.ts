import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tipoproducto } from '../models/tipoproducto';

@Injectable({
  providedIn: 'root'
})
export class TipoproductoService {

  private baseEndPoint='http://localhost:8091/api/inventorysoft/tipoproducto';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Tipoproducto[]>{
  return this.http.get<Tipoproducto[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Tipoproducto>{
  return this.http.get<Tipoproducto>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(tipoproducto:Tipoproducto):Observable<Tipoproducto>{
  return this.http.post<Tipoproducto>('http://localhost:8091/api/inventorysoft/tipoproducto/ingresar', tipoproducto);
}

public eliminarTipoproducto(id: number):Observable<Tipoproducto>{
  return this.http.delete<Tipoproducto>('http://localhost:8091/api/inventorysoft/tipoproducto/eliminar/'+id);
}

public editar(tipoproducto:Tipoproducto):Observable<Tipoproducto>{
  return this.http.put<Tipoproducto>(`${this.baseEndPoint}/modificar/${tipoproducto.idTipoProducto}`, tipoproducto);
}

public getDatos(): Observable <Tipoproducto[]>{
  return this.http.get<Tipoproducto[]>(`${this.baseEndPoint}/listar`)
}


}

