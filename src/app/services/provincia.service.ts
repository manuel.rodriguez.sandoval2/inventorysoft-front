import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Provincia } from '../models/provincia';

@Injectable({
  providedIn: 'root'
})
export class ProvinciaService {

  private baseEndPoint='http://localhost:8091/api/inventorysoft/provincia';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Provincia[]>{
  return this.http.get<Provincia[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Provincia>{
  return this.http.get<Provincia>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(provincia:Provincia):Observable<Provincia>{
  return this.http.post<Provincia>('http://localhost:8091/api/inventorysoft/provincia/ingresar', provincia);
}

public eliminarProvincia(id: number):Observable<Provincia>{
  return this.http.delete<Provincia>('http://localhost:8091/api/inventorysoft/provincia/eliminar/'+id);
}

public editar(provincia:Provincia):Observable<Provincia>{
  return this.http.put<Provincia>(`${this.baseEndPoint}/modificar/${provincia.id}`, provincia);
}

public getDatos(): Observable <Provincia[]>{
  return this.http.get<Provincia[]>(`${this.baseEndPoint}/listar`)
}


}

