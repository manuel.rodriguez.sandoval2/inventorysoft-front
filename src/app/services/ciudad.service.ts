import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ciudad } from '../models/ciudad';

@Injectable({
  providedIn: 'root'
})
export class CiudadService {

  private baseEndPoint='http://localhost:8080/api/comuna';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Ciudad[]>{
  return this.http.get<Ciudad[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Ciudad>{
  return this.http.get<Ciudad>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(ciudad:Ciudad):Observable<Ciudad>{
  return this.http.post<Ciudad>(`${this.baseEndPoint}/ingresar`, ciudad);
}

public eliminarBodega(id: number):Observable<Ciudad>{
  return this.http.delete<Ciudad>(`${this.baseEndPoint}/eliminar/`+id);
}

public editar(ciudad:Ciudad):Observable<Ciudad>{
  return this.http.put<Ciudad>(`${this.baseEndPoint}/modificar/${ciudad.id}`, ciudad);
}

public getDatos(): Observable <Ciudad[]>{
  return this.http.get<Ciudad[]>(`${this.baseEndPoint}/listar`)
}


}
