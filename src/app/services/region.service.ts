import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Region } from '../models/region';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  private baseEndPoint='http://localhost:8091/api/inventorysoft/region';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Region[]>{
  return this.http.get<Region[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Region>{
  return this.http.get<Region>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(region:Region):Observable<Region>{
  return this.http.post<Region>('http://localhost:8091/api/inventorysoft/region/ingresar', region);
}

public eliminarRegion(id: number):Observable<Region>{
  return this.http.delete<Region>('http://localhost:8091/api/inventorysoft/region/eliminar/'+id);
}

public editar(region:Region):Observable<Region>{
  return this.http.put<Region>(`${this.baseEndPoint}/modificar/${region.id}`, region);
}

public getDatos(): Observable <Region[]>{
  return this.http.get<Region[]>(`${this.baseEndPoint}/listar`)
}


}

