export class Merma {

  id:number;
	cantidad:number;
  motivo:string;
  fechaBaja:Date;

	usuario:{
      
        idUsuario:number;
        nombreUsuario:string;
      }
	producto:{
      
        idProducto:number;
        nombreProducto:string;
      }
	bodega:{
      
        id:number;
        nombreBodega:string;
      }
}
