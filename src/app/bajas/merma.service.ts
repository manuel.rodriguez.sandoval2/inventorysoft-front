import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Merma } from './../bajas/merma';


@Injectable({
  providedIn: 'root'
})
export class MermaService {

  private baseEndPoint='http://localhost:8080/api/merma';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Merma[]>{
  return this.http.get<Merma[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Merma>{
  return this.http.get<Merma>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(merma:Merma):Observable<Merma>{
  return this.http.post<Merma>(`${this.baseEndPoint}/ingresar`, merma);
}

public eliminarMerma(id: number):Observable<Merma>{
  return this.http.delete<Merma>(`${this.baseEndPoint}/eliminar`+id);
}

public editar(merma:Merma):Observable<Merma>{
  return this.http.put<Merma>(`${this.baseEndPoint}/modificar/${merma.id}`, merma);
}

public getDatos(): Observable <Merma[]>{
  return this.http.get<Merma[]>(`${this.baseEndPoint}/listar`)
}


}

