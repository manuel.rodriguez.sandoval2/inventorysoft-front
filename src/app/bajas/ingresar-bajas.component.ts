import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Bodega } from '../bodega/bodega';
import { Producto } from '../ingresar/producto';
import { Proveedor } from '../proveedor/proveedor';
import { BodegaService } from '../bodega/bodega.service';
import { ProductoService } from '../ingresar/producto.service';
import { ProveedorService } from '../proveedor/proveedor.service';
import { Merma } from './merma';
import { Usuario } from '../usuarios/usuario';
import { UsuarioService } from '../usuarios/usuario.service';
import { MermaService } from './merma.service';

@Component({
  selector: 'app-ingresar-bajas',
  templateUrl: './ingresar-bajas.component.html',
  styleUrls: ['./ingresar-bajas.component.css']
})
export class IngresarBajasComponent implements OnInit {


  cantidad: number;
  idUsuario: number;
  idProducto: number;
  idBodega: number;

  mermas: Merma[];




  //obtencion de bodegas
  bodegas: Bodega[];
  productos: Producto[];
  usuarios: Usuario[];

  opcionSeleccionado: string = '0';
  verSeleccion: string = '';
  totalRegistros = 0;
  paginaActual = 0;
  totalPorPagina = 5;
  pageSizeOptions: number[] = [3, 8];

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private bodegaService: BodegaService, private productoService: ProductoService,
    private usuarioService: UsuarioService,
    private mermaService: MermaService,
    public http: HttpClient,
    private fb: FormBuilder) { }

  formIngreso: FormGroup = this.fb.group({
    idProducto: [null, Validators.required],
    idbodega: [null, Validators.required],
    cantidad: [null, Validators.required],
    motivo: [null, Validators.required]
  });

  ngOnInit(): void {
    this.formIngreso.valueChanges.subscribe(console.log);
    this.bodegaService.listar().subscribe((b) => (this.bodegas = b));
    this.productoService.listar().subscribe((p) => (this.productos = p));
    this.usuarioService.listar().subscribe((u) => (this.usuarios = u));

    this.calcularRangos();
  }

  paginar(event: PageEvent): void {
    this.paginaActual = event.pageIndex;
    this.totalPorPagina = event.pageSize;
    this.calcularRangos();
  }

  public crear(): void {
    const req: Merma = new Merma();

    req.cantidad = this.formIngreso.value.cantidad;
    req.motivo=this.formIngreso.value.motivo;
    req.producto = {
      idProducto: this.formIngreso.value.idProducto,
      nombreProducto: this.formIngreso.value.nombreProducto
    };
    req.bodega = {
      id: this.formIngreso.value.idbodega,
      nombreBodega: this.formIngreso.value.nombreBodega
    };




    this.mermaService.crear(req).subscribe(

      (cliente) => {
        console.log(cliente);
        Swal.fire(
          'Baja realizada con éxito'
        );
        this.ngOnInit();
      },
      () => {
        console.log("Completed");
      }
    );
  }

  private calcularRangos() {

    this.mermaService.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
      .subscribe(p => {
        this.mermas = p.content as Merma[];
        this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';
      });
  }
}
