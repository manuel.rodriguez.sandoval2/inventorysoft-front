import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Factura } from './factura';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private baseEndPoint='http://localhost:8080/api/factura';
  constructor(private http: HttpClient) { }


  public listar(): Observable <Factura[]>{
  return this.http.get<Factura[]>(`${this.baseEndPoint}/filtrar`);
}

public listarPagina(page:string, size:string): Observable<any>{
  const params =new HttpParams()
  .set('page', page)
  .set('size', size);
  return this.http.get<any>(`${this.baseEndPoint}/listar`, {params: params});

}
public ver(id: number):Observable<Factura>{
  return this.http.get<Factura>(`${this.baseEndPoint}/listar/${id}`);

}
public crear(factura:Factura):Observable<Factura>{
  return this.http.post<Factura>(`${this.baseEndPoint}/ingresar`, factura);
}

public eliminarFactura(id: number):Observable<Factura>{
  return this.http.delete<Factura>(`${this.baseEndPoint}/eliminar/`+id);
}

public editar(factura:Factura):Observable<Factura>{
  return this.http.put<Factura>(`${this.baseEndPoint}/modificar/${factura.id}`, factura);
}

public getDatos(): Observable <Factura[]>{
  return this.http.get<Factura[]>(`${this.baseEndPoint}/listar`)
}


}

