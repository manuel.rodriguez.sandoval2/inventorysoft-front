import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import Swal from 'sweetalert2';
import { Cliente } from '../clientes/cliente';
import { ClienteService } from '../clientes/cliente.service';
import { Producto } from '../ingresar/producto';
import { ProductoService } from '../ingresar/producto.service';
import { Factura } from './factura';
import { FacturaService } from './factura.service';

interface TipoDocumento {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})
export class FacturaComponent implements OnInit {

  clientes: Cliente[];
  productos: Producto[];
  facturas:Factura[];



  opcionSeleccionado: string = '0';
  verSeleccion: string = '';
  totalRegistros = 0;
  paginaActual = 0;
  totalPorPagina = 5;
  pageSizeOptions: number[] = [3, 8];

 

  cantidad: number;
  numeroDocumento: number;
  precio: number;
  tipoDocumento : TipoDocumento[]= [
    {value:'Guia', viewValue:'Guia'},
    {value:'Factura', viewValue:'Factura'} 
  ];
  idCliente: number;
  idProducto: number;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private clienteService: ClienteService, private productoService: ProductoService,
              private facturaService: FacturaService,
    public http: HttpClient,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.clienteService.listar().subscribe((c) => (this.clientes = c));
    this.productoService.listar().subscribe((p) => (this.productos = p));
    this.formIngreso.valueChanges.subscribe(console.log);
  //  this.calcularRangos();
  }

  formIngreso: FormGroup = this.fb.group({

    cantidad: [null, Validators.required],
    numeroDocumento: [null, Validators.required],
    precio: [null, Validators.required],
    tipoDocumento: [null, Validators.required],
    idProducto: [null, Validators.required],
    idCliente: [null, Validators.required]
  });

  paginar(event: PageEvent): void {
    this.paginaActual = event.pageIndex;
    this.totalPorPagina = event.pageSize;
  //  this.calcularRangos();
  }

  public crear(): void {
    const req: Factura = new Factura();


    req.cantidad = this.formIngreso.value.cantidad;
    req.numeroDocumento = this.formIngreso.value.numeroDocumento;
    req.precio = this.formIngreso.value.precio;
    req.tipoDocumento = this.formIngreso.value.tipoDocumento;
    req.cliente = {
      id: this.formIngreso.value.idCliente
      
    };
    req.producto = {
      idProducto: this.formIngreso.value.idProducto


    };


    this.facturaService.crear(req).subscribe(
      
      (cliente) => {
        console.log(cliente);
        Swal.fire(

          'Documento ingresado con éxito'
        );
        this.ngOnInit();
      },
      
      () => {
        console.log("Completed");
      }
    );
  }
 /* private calcularRangos() {


    this.facturaService.listarPagina(this.paginaActual.toString(), this.totalPorPagina.toString())
      .subscribe(p => {
        this.facturas = p.content as Factura[];
        this.totalRegistros = p.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página:';

      });
    console.log(Factura);*/
  }

  /*eliminar(factura: Factura) {
    if (confirm(`¿Seguro que desea eliminar a ${factura}?`))
      console.log(factura.id,
        factura.nombreCliente);

    this.service.eliminar(factura.id).subscribe(() => {
      this.clientes = this.clientes.filter(u => u !== factura);

      alert(`Factura ${factura.nombreCliente} eliminado con éxito `);


    });*/
  





