export class Factura {

  id: number;
  cantidad: number;
  numeroDocumento: number;
  tipoDocumento: string;
  precio: number;
  cliente: {

    id: number;
  }
  producto: {

    idProducto: number;
  }
}
